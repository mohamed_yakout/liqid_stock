require 'date'

class Stock
	attr_accessor :name, :date, :open, :close, :low, :high, :drawdown, :rate

	def initialize stock_details
		@name = stock_details[:name]
		@date = stock_details[:date]
		@open = stock_details[:open]
		@close = stock_details[:close]
		@low = stock_details[:low]
		@high = stock_details[:high]
	end

	# setter method to set drawdown value 
	def calculate_drawdown
		self.drawdown = ((self.low - self.high)  * 100.0 / self.low).round(1)
	end

	# format date as required output date
	def get_formatted_date
		Date.parse(self.date).strftime("%d\.%m\.%y")
	end

	# Get stock details statement
	def get_stock_details_statement
		"#{self.get_formatted_date}: Closed at #{self.close} (#{self.low} ~ #{self.high})"
	end

	# get drawdown statement
	def get_drawdown_statement
		"#{self.drawdown}% (#{self.high} on #{self.get_formatted_date} -> #{self.low} on #{self.get_formatted_date})"
	end
end



# calculate rate of return
def calculate_rate_return final_val, init_val
	((final_val - init_val) * 100.0 / init_val).round(1)
end

# Check string is valid date or not
def is_date date
	Date.parse date rescue false
end



