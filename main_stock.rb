require 'net/http'
require 'date'
require 'json'
require './stock'

# Get List dates in gived range
def get_dates_list start_date, end_date
	(Date.parse(start_date) .. Date.parse(end_date)).map{|d| d.strftime("%Y-%m-%d")}
end

# This method convert stock details array to hash
def get_stock_details stock_details_array
	{
		name: stock_details_array[0], 
		date: stock_details_array[1], 
		open: stock_details_array[2], 
		high: stock_details_array[3], 
		low: stock_details_array[4], 
		close: stock_details_array[5], 
		volume: stock_details_array[6], 
		dividends: stock_details_array[7], 
		closeunadj: stock_details_array[8], 
		lastupdates: stock_details_array[9]
	}
end

# This Method parse input
def parse_input array_inputs
	input_hash = {
		stock_names: [], 
		start_date: nil, 
		end_date: nil
	}

	len = array_inputs.size
	index = 1
	if array_inputs[len - 4] == '-'
		index = len - 3
		input_hash[:end_date] = "#{array_inputs[index]} #{array_inputs[index + 1]} #{array_inputs[index + 2]}"
		index = len - 7
	else
		index = len - 3
	end
	input_hash[:start_date] = "#{array_inputs[index]} #{array_inputs[index + 1]} #{array_inputs[index + 2]}"

	input_hash[:end_date] ||= Date.today.to_s

	input_hash[:stock_names] = array_inputs[0...index]

	input_hash
end

API_KEY = ENV.to_h["API_KEY"]

input_hash = parse_input ARGV.to_a

stock_names = input_hash[:stock_names]

dates = get_dates_list input_hash[:start_date], input_hash[:end_date]

# Send Request
stock_api = "https://www.quandl.com/api/v3/datatables/SHARADAR/SEP"

uri = URI(stock_api)
params = { date: dates.join(","), ticker: stock_names.join(","), api_key: API_KEY }
uri.query = URI.encode_www_form(params)

res = Net::HTTP.get_response(uri)

if res.is_a?(Net::HTTPSuccess)
	stock_array = JSON.parse(res.body)["datatable"]["data"]

	stocks_list = []

	# Output
	# Loop to stock array build array of stocks
	# Set drawdown value of each stock
	stock_array.each do |stock_details_array|
		stock_details = get_stock_details stock_details_array
		stock = Stock.new(stock_details)
		stocks_list << stock
		puts stock.get_stock_details_statement
		stock.calculate_drawdown
	end

	num_stocks = stocks_list.size

	# Print First 3 Drawdowns
	puts "\nFirst 3 Drawdowns:\n"

	sorted_stocks_by_drawdown = stocks_list.sort { |a, b|  a.drawdown <=> b.drawdown }
	sorted_stocks_by_drawdown.first(3).each do |stock|
		puts stock.get_drawdown_statement
	end

	# Print Maximum drawdown
	puts "\nMaximum drawdown: #{sorted_stocks_by_drawdown.first.get_drawdown_statement}\n"

	# Calculate Diff & rate of return
	total_rate_return = calculate_rate_return stocks_list.last.close, stocks_list.first.close

	diff_stock_price = stocks_list.last.close - stocks_list.first.close

	# Print rate of return
	puts "\nReturn: #{diff_stock_price} [#{total_rate_return}%] (#{stocks_list.first.close} on #{stocks_list.first.get_formatted_date} -> #{stocks_list.last.close} on #{stocks_list.last.get_formatted_date})"

else
	puts res.body
end