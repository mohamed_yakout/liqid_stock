# Readme

Clone Project and run sample: 

```
> git clone https://mohamed_yakout@bitbucket.org/mohamed_yakout/liqid_stock.git
> cd liqid_stock
```

To get Stocks Prices, run the following: 

```
> API_KEY=XXX ruby main_stock.rb AAPL Jan 1 2017
> API_KEY=XXX ruby main_stock.rb AAPL Jan 1 2017 - Jan 1 2018
```

---

## Done List: 

1. Read params from command line.
2. Send Request to quandl API. 
3. Add methods to calculate drawdown & rate of return. 
4. Diplay API Errors in the output
5. Add the code in git repo with multiple commits. 

---

## TODO List: 

1. Send Email/Slack Notification. 
	1.1. Create Gmail APP. 
	1.2. Add Ruby Script with Auth of App to send mails.
2. Handle Wrong Input Format. 
3. Add Automated Tests.

---

## Architecture Design:

- stock.rb file has class stock and helper methods. 
- main_stock.rb file has main script to send request to API & puts output in required format. 

## Code Quality:

- Code has comments in most parts. 

## Performance: 

- If we need to handle big date range, I need to handle it by:
	1. send multiple requests. 
	2. calculate responses in database. 
	3. sort database by drawdown attribute. 
	4. run display code to print the output. 
